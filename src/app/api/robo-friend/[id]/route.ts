import { NextResponse } from "next/server";

export async function GET(request: Request) {
  const id = request.url.split("/")[request.url.split("/").length - 1];
  const res = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
  const data = await res.json();
  return NextResponse.json({ data });
}
