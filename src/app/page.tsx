import App from "../../components/App";
import { Robot } from "../../components/type";


const getRobots = async (): Promise<Array<Robot>> => {
  try {
    const res = await fetch(`${process.env.DOMAIN_URL}/api`, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    const data = await res.json();
    return data.data;
  } catch (error) {
    return [];
  }
};

export default async function Page() {
  const data = await getRobots();
  return <App roboFriends={data} />;
}
