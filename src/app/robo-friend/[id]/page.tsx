import SingleRoboFriend from "../../../../components/SingleRoboFriend";
import { Robot } from "../../../../components/type";

const getSpecificRobot = async (id: number): Promise<Robot> => {
  try {
    const res = await fetch(`${process.env.DOMAIN_URL}/api/robo-friend/${id}`, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!res.ok) {
      throw res;
    }
    const data = await res.json();
    return data.data;
  } catch (error) {
    throw error;
  }
};

export default async function Page({
  params: { id },
}: {
  params: { id: number };
}) {
  const specificRobot = await getSpecificRobot(id);
  return (
    <SingleRoboFriend
      id={id}
      email={specificRobot.email}
      name={specificRobot.name}
      website={specificRobot.website}
      showMore
    />
  );
}
