"use client";

import React from "react";
import Card from "./Card";

const SingleRoboFriend = ({
  id,
  name,
  email,
  showMore,
  website,
}: {
  id: number;
  name: string;
  email: string;
  showMore?: boolean;
  website: string;
}) => {
  return (
    <Card
      name={name}
      email={email}
      id={id}
      showMore={showMore}
      website={website}
    />
  );
};

export default SingleRoboFriend;
