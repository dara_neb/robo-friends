import Image from "next/image";
import React from "react";

const Card = ({
  name,
  email,
  id,
  showMore,
  onPress,
  website,
}: {
  name: string;
  email: string;
  id: number;
  showMore?: boolean;
  onPress?: () => void;
  website?: string;
}) => {
  return (
    <div
      className="tc grow bg-light-green br3 pa3 ma2 dib bw2 shadow-5"
      onClick={() => onPress && onPress()}
    >
      <Image
        alt="robots"
        src={`https://robohash.org/${id}?size=200x200`}
        width={200}
        height={200}
      />
      <div>
        <h2>{name}</h2>
        <p>{email}</p>
        {showMore && (
          <>
            <p>{website}</p>
          </>
        )}
      </div>
    </div>
  );
};

export default Card;
