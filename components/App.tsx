"use client";

import React, { useState } from "react";
import CardList from "./CardList";
import "tachyons";
import SearchBox from "./SearchBox";
import Scroll from "./Scroll";
import "./App.css";
import { Robot } from "./type";

interface Props {
  roboFriends: Array<Robot>;
}

const App = (props: Props) => {
  const { roboFriends } = props;
  const [searchfield, setSearchfield] = useState("");
  // const [robots, setRobots] = useState([]);
  // console.log("robots", robots);

  // useEffect(() => {
  //   fetch("https://jsonplaceholder.typicode.com/users")
  //     .then((response) => response.json())
  //     .then((users) => {
  //       setRobots(users);
  //     });
  // }, []);

  const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchfield(event.target.value);
  };

  const filteredRobots = roboFriends.filter((robot) => {
    return robot?.name?.toLowerCase().includes(searchfield.toLowerCase());
  });
  return !roboFriends.length ? (
    <h1>Loading</h1>
  ) : (
    <div className="tc">
      <h1 className="f1">RoboFriends</h1>
      <SearchBox searchChange={onSearchChange} />
      <Scroll>
        <CardList robots={filteredRobots} />
      </Scroll>
    </div>
  );
};

export default App;
