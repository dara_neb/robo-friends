import React from "react";
import Card from "./Card";
import { Robot } from "./type";
import { useRouter } from "next/navigation";

interface Props {
  robots: Array<Robot>;
}

const CardList = (props: Props) => {
  const router = useRouter();
  const { robots } = props;
  return (
    <div>
      {robots.map((user: Robot, i: number) => {
        return (
          <Card
            key={i}
            id={robots[i].id}
            name={robots[i].name}
            email={robots[i].email}
            onPress={() => router.push(`/robo-friend/${user.id}`)}
          />
        );
      })}
    </div>
  );
};

export default CardList;
