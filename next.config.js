/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['robohash.org'],
    remotePatterns: [
      {
        protocol: "https",
        hostname: "robohash.org",
        port: "",
        pathname: "${id}?size=200x200",
      },
    ],
  },
};

module.exports = nextConfig;
